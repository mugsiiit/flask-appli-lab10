## Objective

Objective of the lab is to cover basic Front-end routing using page.js, various concepts involved in maintaining sessions which in turn help in authentication and a structured approach to create a basic Todo application.

## Structure of the code

### File Description

-- run.py –> The main python file which runs the server <br/>
-- config.py –> Sets the config for the Flask object <br/>
-- app/ –> All the code for the application (Controllers / Models / Views / Static files) <br/>
-- app/templates/ –> Contains only one index.html file which loads all the script files required for the application. We are about to see an application that does not reload the pages on form submissions. Everything is handled by javascript without explicit refreshing of pages. Only the html content of this index.html file is updated according to the user action <br/>
-- app/static/ –> Contains external vendor's css and js files along with javascript routing code defined by us <br/>
-- app/static/templates/ –> Contains templates for the application which will be rendered into a div in the single index.html that we defined before <br/>
-- app/static/js/routes.js –> Front Routing is handled here. Which route invokes which js function is explicitly handled here. The syntax of this file is governed by page.js <br/>
-- app/static/js/helpers.js –> Defines a helper for form submission which prevents the default page load on submission of forms and instead performs an AJAX call according to the attributes provided in the html form and the form data. <br/>
-- app/todo/ –> Directory containing controllers and models for todo <br/>
-- app/user/ –> Directory containing controllers and models for user (Login / Logout handled here) <br/>
-- app/__init__.py –> Contains initializing for Flask object and handles importing of controllers and models from various modules defined above <br/>

### Blueprints

-- Flask uses a concept of blueprints for making application components and supporting common patterns within an application or across applications. Blueprints can greatly simplify how large applications work and provide a central means for Flask extensions to register operations on applications. A Blueprint object works similarly to a Flask application object, but it is not actually an application. Rather it is a blueprint of how to construct or extend an application. <br/>
-- We use Blueprints to modularize our application and provide a url_prefix /api to every route that we define in the controller file. <br/>
-- There are many other reasons to use blueprints (http://flask.pocoo.org/docs/0.12/blueprints/) <br/>

### index.html

-- This is the default template that is loaded on every request. Instead of routing files from the backend we are aiming to perform frontend routing and hence we only render one html file once. <br/>
-- Any user action only changes the content of the html file and does not refresh the page. <br/>
-- Since this is the only page that is loaded, we need to load all the scripts in this page. <br/>

### Individual templates

-- Can be found in - app/static/templates <br/>
-- We will put the contents of these templates in the main index.html according to user action. <br/>
-- Logic for this is handled by view-manager.js which internally invokes auth-manager.js and todo-manager.js. <br/>

### Bringing it together

-- User requests a page. <br/>
-- routes.js decides on the basis of this which js function needs to be called. <br/>
-- The manager js files then make the respective AJAX calls to the API defined in the backend <br/>
-- Backend performs the auth checks from the session object and authorizes the request and returns the response based on the request. <br/>
-- This response is parsed by view-manager.js and then put in index.html <br/>

## Sessions 

-- Our final aim is to prevent certain endpoints / routes to be accessed from not-logged in users. <br/>
-- A session can be defined as a server-side storage of information that is desired to persist throughout the user's interaction with the web site or web application. <br/>
-- Instead of storing large and constantly changing information via cookies in the user's browser, only a unique identifier is stored on the client side (called a "session id"). This session id is passed to the web server every time the browser makes an HTTP request (ie a page link or AJAX request). The web application pairs this session id with it's internal database and retrieves the stored variables for use by the requested page. <br/>
-- When you work with an application, you open it, do some changes, and then you close it. This is much like a Session. The computer knows who you are. It knows when you start the application and when you end. But on the internet there is one problem: the web server does not know who you are or what you do, because the HTTP address doesn't maintain state. Session variables solve this problem by storing user information to be used across multiple pages (e.g. username, favorite color, etc). By default, session variables last until the user closes the browser. <br/>
-- Flask provides a session object inside which you can store various details about a particular session. In the Todo application you can see the usage of this object for login / logout in app/users/controllers.py file. <br/>

For login - <br/>
session['user_id'] = user.id


For logout - <br/>
session.pop('user_id')


For checking if a user is logged in (A decorator requires_auth for the same is defined in app/__init__.py) - <br/>
if 'user_id' not in session:<br/>
   </t> return jsonify(message="Unauthorized", success=False), 401


-- Hence to prevent certain users from certain routes all we need to do is mention @requires_auth and it will return a 401 response status for users who are not authorized to view that page. <br/>

-- Examples of the usage can be found in app/todo/controllers.py <br/>


## Routing (routes.js)

Sample route explanation - <br/>

page('/todo/create', authManager.requiresAuthentication, todoManager.create);<br/>


-- This says if a request is received on /todo/create, then execute authManager.requiresAuthentication first and then todoManager.create The thing to note here is that this list of callbacks can be terminated at any particular function i.e. our goal is to restrict access to todoManager based on user is logged in or not.  <br/>
<br/>
var requiresAuthentication = function(ctx, next) {<br/>
    getLoginState(function (state) {<br/>
        if (state && state.loggedIn) {<br/>
            next();<br/>
        } else {<br/>
            intendedPath = ctx.path;<br/>
            //showLogin();<br/>
            page('/login');<br/>
        }<br/>
    });<br/>
};<br/>

-- The next is a callback calling of which will result in execution of todoManager.create, if the user is not logged in then he/she will be redirected to '/login' after saving the current path in intendedPath. <br/>
-- This intendedPath will be used for redirecting a user after successful login.  <br/><br/>
var loggedIn = function (user) {<br/>
    loginState = {};<br/>
    loginState.loggedIn = true;<br/>
    loginState.user = user;<br/>
    page(intendedPath ? intendedPath : '/');<br/>
};<br/>
